using System;
using System.Collections.Generic;
using UnityEngine;

public class Tool:MonoBehaviour
{
    [SerializeField] private string _scene = "MainScene";
    [SerializeField] private string _ui = "keys";
    [SerializeField] private List<string> _list = new() {};

    public string Scene => _scene;
    public string UI => _ui;
    public string GetLin() => String.Join("", _list.ToArray());
}