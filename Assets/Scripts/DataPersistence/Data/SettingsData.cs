using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsData
{
    public bool _muteSoundEffects;
    public bool _muteMusic;
}
